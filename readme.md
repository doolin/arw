# README - ActiveRecord Workbook



2019-10-06: Haven't had time to work on this project. The next goal
is to ensure all the examples run with the latest version of
ActiveRecorsd.

Biggums J. Smallhand

## Chapter 0 Precis

Why another book on Rails?

That's a good question with many answers.

The first and most important answer is this book scratches an annoying
itch on the author's behalf. It's the book he wishes he could have
simply purchased. Another answer might be that this book takes a
different perspective than most othor books about Rails, focusing
specifically on Rails' ActiveRecord gem. Yet another reason might
be that this book makes an attempt to bridge Rails' ActiveRecord
abstraction with the underlying database technology. The SQL and
ActiveRecord's Arel are considered together and in some detail.
Finally, the author isn't aware of any other books making a deep
dive specifically into ActiveRecord. If there is a market opportunity,
hopefully this book will find that opportunity.

The central premise of the book is introducing  _relational prototyping_
to get fast insights into the behavior of a schema and its
associations before (or during) implementation in Rails'
ActiveRecord.

There is a "test-the-framework" aspect to this as well. While
testing frameworks is almost universally derided in application
test suites, for what follows in this book, having a concrete
example of how `accepts_nested_attributes_for` works is pretty
convenient. Over a software development lifecycle, what may be seen as
fundamental framework behavior may change. It's nice to have a
non-critical example to isolate such changes, and work out methods for
updating.

The author makes no claims of specific expertise with ActiveRecord,
Rails, Ruby, or programming in general.

### A fundamental conundrum

SQL being a means to an end, the practitioner is often confronted
with needs which either do not have a purely SQL solution, or the
SQL solution which is available is not portable. For example, not
all databases have window functions, or triggers. While window
functions can often, with difficulty, be conducted in pure SQL,
the resulting queries may not be easy to understand. Without triggers,
application logic must serve the same need.

The conuundrum is where best to make this division between what is
native to the database, and what must be handled in application.
It seems pretty clear that aggregation resulting in the infamous
N + 1 problem should be moved to SQL. It's less clear, for example,
how one might compute moving averages.


### The examples

It seems traditional that the SQL acolyte master the "business
trivium" of Customer, Order and Invoice.

We're not doing that here as customers, orders and invoices have been
covered in great detail everywhere else, and the author hasn't actually
written an invoicing system.

Instead, the examples are drawn from science, engineering, and
contemporary recreation, hopefully of more intrinsic interest to
technically inclined readers.

### A brief look ahead

1. Big Giant File
2. Tiny Little Files
3. First Normal Form
4. Second Normal Form
5. Third Normal Form


## Chapter 1 Big Giant File

We start with putting everything into a Big Giant File.
This makes it easier to see everything going on. Later,
the Big Giant File can be refactored into its component
parts (Tiny Little Files) and relocated appropriately.

Seeing how the Big Giant File has its various bits distributed
into application files helps understand the control of flow
in a larger application, for example, Rails.


## Chapter 2 Tiny Little Files

While the Big Giant File has many advantages, it fast becomes
unweildy as the complexity of the investigation grows.

We'll start with taking apart one of the Big Giant Files and placing
all its little pieces in a single, convenient location.

We'll go forward with some variation of Tiny Little Files.


### A brief bit of theory

### First Normal Form

### Second Normal Form

### Third Normal Form and BCNF


## ActiveRecord associations

This is probably what most readers have been looking for.


### Belongs to

This is the simplest of all associations, in every way.

#### Rails and foreign keys

The professional literature derides using sequential integers as primary
keys, but that's what Rails does by default, so that's what we're going
to do as well. Or rather, Rails does it for us automatically.

Continuing, consider a User and that User's Profile.

### Has many



## Chapter Getting Ugly

## Ruby yard installation

From `bundle install`:

```
--------------------------------------------------------------------------------
As of YARD v0.9.2:

RubyGems "--document=yri,yard" hooks are now supported. You can
auto-configure YARD to automatically build the yri index for
installed gems by typing:

    $ yard config --gem-install-yri

See `yard config --help` for more information on RubyGems install hooks.

You can also add the following to your .gemspec to have YARD document
your gem on install:

    spec.metadata["yard.run"] = "yri" # use "yard" to build full HTML docs.

--------------------------------------------------------------------------------
```
