-- Just to get started...

-- The database scheme consists of four tables:

-- Product(maker, model, type)
create table if not exists Product(
  maker varchar(10),
  model varchar(50),
  type varchar(50)
);

-- PC(code, model, speed, ram, hd, cd, price)
create table if not exists PC(
  code int,
  model varchar(50),
  speed smallint,
  ram smallint,
  hd real,
  cd varchar(10),
  price float -- money type
);

-- Laptop(code, model, speed, ram, hd, screen, price)
create table if not exists Laptop(
  code int,
  model varchar(50),
  speed smallint,
  ram smallint,
  hd real,
  cd varchar(10),
  price float, -- money type
  screen int -- tinyint type
);


-- Printer(code, model, color, type, price)
create table if not exists Printer(
  code int,
  model varchar(50),
  color char(1),
  type varchar(10),
  price float -- money type
);

-- The Product table contains data on the maker, model number, and type of
-- product ('PC', 'Laptop', or 'Printer'). It is assumed that model numbers
-- in the Product table are unique for all makers and product types.
-- Each personal computer in the PC table is unambiguously identified by a
-- unique code, and is additionally characterized by its model (foreign key
-- referring to the Product table), processor speed (in MHz) – speed field,
-- RAM capacity (in Mb) - ram, hard disk drive capacity (in Gb) – hd, CD-ROM
-- speed (e.g, '4x') - cd, and its price. The Laptop table is similar to the
-- PC table, except that instead of the CD-ROM speed, it contains the screen
-- size (in inches) – screen.

-- For each printer model in the Printer table,
-- its output type (‘y’ for color and ‘n’ for monochrome) – color field,
-- printing technology ('Laser', 'Jet', or 'Matrix') – type, and price are
-- specified.

-- code, model, speed, ram, hd, cd, price
insert into PC (code, model, speed, ram, hd, cd, price) values (1,  1232, 500, 64,  5.0, '12x', 600.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (10, 1260, 500, 32,  10.0, '12x', 350.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (11, 1233, 900, 128, 40.0, '40x', 980.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (12, 1233, 800, 128, 20.0, '50x', 970.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (2,  1121, 750, 128, 14.0, '40x', 850.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (3,  1233, 500, 64,  5.0, '12x', 600.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (4,  1121, 600, 128, 14.0, '40x', 850.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (5,  1121, 600, 128, 8.0, '40x', 850.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (6,  1233, 750, 128, 20.0, '50x', 950.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (7,  1232, 500, 32,  10.0, '12x', 400.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (8,  1232, 450, 64,  8.0, '24x', 350.0000);
insert into PC (code, model, speed, ram, hd, cd, price) values (9,  1232, 450, 32,  10.0, '24x', 350.0000);

select '' AS '';
select 'Query 1' as '';
select model, speed, hd from PC where price < 500;


insert into Product (maker, model, type) values ('A', 1232, 'PC');
insert into Product (maker, model, type) values ('A', 1233, 'PC');
insert into Product (maker, model, type) values ('A', 1276, 'Printer');
insert into Product (maker, model, type) values ('A', 1298, 'Laptop');
insert into Product (maker, model, type) values ('A', 1401, 'Printer');
insert into Product (maker, model, type) values ('A', 1408, 'Printer');
insert into Product (maker, model, type) values ('A', 1752, 'Laptop');
insert into Product (maker, model, type) values ('B', 1121, 'PC');
insert into Product (maker, model, type) values ('B', 1750, 'Laptop');
insert into Product (maker, model, type) values ('C', 1321, 'Laptop');
insert into Product (maker, model, type) values ('D', 1288, 'Printer');
insert into Product (maker, model, type) values ('D', 1433, 'Printer');
insert into Product (maker, model, type) values ('E', 1260, 'PC');
insert into Product (maker, model, type) values ('E', 1434, 'Printer');
insert into Product (maker, model, type) values ('E', 2112, 'PC');
insert into Product (maker, model, type) values ('E', 2113, 'PC');

select '' AS '';
select 'Query 2' as '';
select distinct maker from Product where type = 'Printer';


insert into Laptop (code, model, speed, ram, hd, price, screen) values (1, 1298, 350, 32, 4.0, 700.0000, 11);
insert into Laptop (code, model, speed, ram, hd, price, screen) values (2, 1321, 500, 64, 8.0, 970.0000, 12);
insert into Laptop (code, model, speed, ram, hd, price, screen) values (3, 1750, 750, 128, 12.0, 1200.0000, 14);
insert into Laptop (code, model, speed, ram, hd, price, screen) values (4, 1298, 600, 64, 10.0, 1050.0000, 15);
insert into Laptop (code, model, speed, ram, hd, price, screen) values (5, 1752, 750, 128, 10.0, 1150.0000, 14);
insert into Laptop (code, model, speed, ram, hd, price, screen) values (6, 1298, 450, 64, 10.0, 950.0000, 12);

select '' AS '';
select 'Query 3' as '';
select model, ram, screen from Laptop where price > 1000;


insert into Printer (code, model, color, type, price) values (1, 1276, 'n', 'Laser', 400.0000);
insert into Printer (code, model, color, type, price) values (2, 1433, 'y', 'Jet', 270.0000);
insert into Printer (code, model, color, type, price) values (3, 1434, 'y', 'Jet', 290.0000);
insert into Printer (code, model, color, type, price) values (4, 1401, 'n', 'Matrix', 150.0000);
insert into Printer (code, model, color, type, price) values (5, 1408, 'n', 'Matrix', 270.0000);
insert into Printer (code, model, color, type, price) values (6, 1288, 'n', 'Laser', 400.0000);

select '' AS '';
select 'Query 4' as '';
select * from Printer where color = 'y';


select '' AS '';
select 'Query 5' as '';
select model, speed, hd from PC where price < 600 and cd in ('12x', '24x');
-- model, speed, hd
-- 1232, 500, 10.0
-- 1232, 450, 8.0
-- 1232, 450, 10.0
-- 1260, 500, 10.0


-- http://sql-ex.ru/learn_exercises.php?LN=6
-- "For each maker producing laptops with a hard drive capacity of
-- 10 Gb or higher, find the speed of such laptops. Result set: maker, speed."

select '' AS '';
select 'Query 6' as '';
SELECT DISTINCT Product.maker, Laptop.speed
FROM Product, Laptop
WHERE Laptop.hd >= 10 AND
 type IN(SELECT type
   FROM Product
   WHERE type = 'Laptop');


drop table PC;
drop table Product;
drop table Laptop;
drop table Printer;
