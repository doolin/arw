#!/usr/bin/env ruby
# frozen_string_literal: true

# Denote one game of many as a primary
# game.

require 'active_record'
require 'pry'

require 'rspec/autorun'

DB_SPEC = {
  adapter: 'sqlite3',
  database: ':memory:'
}.freeze
ActiveRecord::Base.establish_connection(DB_SPEC)

class Games < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name
      t.belongs_to :person
      t.timestamp
    end
  end
end

class Persons < ActiveRecord::Migration[5.2]
  def change
    create_table :persons do |t|
      t.string :name
      t.timestamps
    end
  end
end

class FavoriteGames < ActiveRecord::Migration[5.2]
  def change
    create_table :favorite_games do |t|
      t.belongs_to :person, index: true
      t.belongs_to :game, index: true
      t.timestamps
    end
  end
end

ActiveRecord::Migrator.new(:change, Games.new.change).migrate unless Games.table_exists?(:games)
ActiveRecord::Migrator.new(:change, Persons.new.change).migrate unless Persons.table_exists?(:persons)
ActiveRecord::Migrator.new(:change, FavoriteGames.new.change).migrate unless FavoriteGames.table_exists?(:favorite_games)

class Person < ActiveRecord::Base
  self.table_name = 'persons'

  validates :name, presence: true

  has_one :favorite_game, through: :games
  has_many :games

  scope :built_games, ->(name) { joins(:games).where(games: { name: name }).pluck(:name) }

  def favorite_game=(game)
    binding.pry
    return if favorite_game == game
    if favorite_game.nil?
      FavoriteGame.create!(person: self, game: game)
      return
    end
    favorite_game.update!(game_id: game.id)
  end
end

class Game < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  belongs_to :person
  has_one :favorite_game

  scope :built_by, ->(name) { joins(:person).where(persons: { name: name }).distinct(:name).pluck(:name) }
end

class FavoriteGame < ActiveRecord::Base
  belongs_to :game
  belongs_to :person
end

dave = Person.create!(name: 'dave')

boats = Game.create!(name: 'boats', person: dave)
Game.create!(name: 'cars', person: dave)
art = Game.create!(name: 'art', person: dave)

RSpec.describe Person do
  describe '.find_by' do
    example 'dave' do
      expect(Person.find_by(name: 'dave').name).to eq 'dave'
    end
  end

  describe '.built_games' do
    example 'by name' do
      expected = %w[dave]
      actual = Person.built_games('cars')
      expect(actual).to eq expected
    end
  end
end

RSpec.describe Game do
  describe '.built_by' do
    it 'shows games person has built' do
      expected = %w[boats cars art]
      actual = Game.built_by('dave')
      expect(actual).to match_array(expected)
    end
  end
end

RSpec.describe FavoriteGame do
  it 'set primary game' do
    dave.favorite_game = boats
    expect(dave.favorite_game).to eq boats
    FavoriteGame.destroy_all
  end

  # TODO: enforce uniqueness on person_id
  xit 'replaces primary game' do
    previous = FavoriteGame.create!(person: dave, game: boats)
    expect(dave.favorite_game).to be previous
    current = FavoriteGame.create!(person: dave, game: art)
    expect(FavoriteGame.count).to be 1
    # binding.pry
    expect(dave.favorite_game).to be current
    FavoriteGame.destroy_all
  end

  it 'updates primary game' do
    FavoriteGame.destroy_all
    primary = FavoriteGame.create!(person: dave, game: boats)
    expect(FavoriteGame.count).to be 1
    binding.pry
    dave.favorite_game = art
    expect(FavoriteGame.count).to be 1
    expect(dave.favorite_game.game.name).to eq art.name
    expect(FavoriteGame.count).to be 1
    FavoriteGame.destroy_all
  end
end

# More questions to ask:
# * How many times has a person built some particular game?
# * Which  games are two person only?
# * Which games were built by only two distinct people?
