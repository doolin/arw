#!/usr/bin/env ruby
# frozen_string_literal: true

# Denote one game of many as a primary
# game.

require 'active_record'
require 'pry'

require 'rspec/autorun'

DB_SPEC = {
  adapter: 'sqlite3',
  # database: ':memory:'
  database: 'favorite.db'
}.freeze
ActiveRecord::Base.establish_connection(DB_SPEC)

class Games < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name
      # t.belongs_to :favoritable
      # t.string :favoritable_type
      t.belongs_to :person
      t.timestamp
    end
  end
end

class Persons < ActiveRecord::Migration[5.2]
  def change
    create_table :persons do |t|
      t.string :name
      t.integer :favorite_game_id
      t.timestamps
    end
    add_index :persons, :favorite_game_id
    # add_foreign_key :persons, :games, column: :favorite_game_id
  end
end

ActiveRecord::Migrator.new(:change, Games.new.change).migrate unless Games.table_exists?(:games)
ActiveRecord::Migrator.new(:change, Persons.new.change).migrate unless Persons.table_exists?(:persons)

class Person < ActiveRecord::Base
  self.table_name = 'persons'

  validates :name, presence: true

  has_many :games
  belongs_to :favorite_game, class_name: 'Game', optional: true

  scope :owned_games, ->(name) { joins(:games).where(games: { name: name }).pluck(:name) }
end

class Game < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  belongs_to :person
  # belongs_to :favoritable # , polymorphic: true
  # has_one :favorite_game, foreign_key: 'favorite_game_id', foreign_type: :person # , class_name: 'Person'

  scope :owners, ->(name) { joins(:person).where(persons: { name: name }).distinct(:name).pluck(:name) }
end

dave = Person.create!(name: 'dave')
al = Person.create!(name: 'al')

boats = Game.create!(name: 'boats', person: dave)
Game.create!(name: 'cars', person: dave)
art = Game.create!(name: 'art', person: dave)
Game.create!(name: 'jazz', person: al)

RSpec.describe Person do
  describe '.find_by' do
    example 'dave' do
      expect(Person.find_by(name: 'dave').name).to eq 'dave'
    end
  end

  describe '.built_games' do
    example 'by name' do
      expected = %w[dave]
      actual = Person.owned_games('cars')
      expect(actual).to eq expected
    end
  end
end

RSpec.describe Game do
  context 'number of games' do
    it 'counts games' do
      expect(dave.games.count).to eq 3
    end
  end

  describe '.built_by' do
    it 'shows games person has built' do
      expected = %w[boats cars art]
      actual = Game.owners('dave')
      expect(actual).to match_array(expected)
    end
  end
end

RSpec.describe "FavoriteGame" do
  it 'set favorite game' do
    dave.update!(favorite_game: boats)
    expect(dave.favorite_game).to eq boats
    expect(dave.games.count).to eq 3
    expect(Game.count).to eq 4
    boats.destroy
    expect(Game.count).to eq 3
    dave.reload
    expect(dave.favorite_game).to be nil
  end

  it 'updates primary game' do
    dave.update!(favorite_game: boats)
    expect(dave.favorite_game.id).to be boats.id

    dave.update!(favorite_game: art)
    expect(dave.favorite_game.id).to eq art.id
    expect(Game.count).to eq 3
  end
end
