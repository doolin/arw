# ActiveRecord update_attributes!


From the [ActiveRecord::Persistence
documentation](http://api.rubyonrails.org/classes/ActiveRecord/Persistence.html#method-i-update_attributes-21)
we see that `update_attributes!` is an alias for `update!`:

<blockquote>
Updates its receiver just like `update` but calls save! instead of
save, so an exception is raised if the record is invalid.
</blockquote>

For `update`:

<blockquote>
Updates the attributes of the model from the passed-in hash and
saves the record, all wrapped in a transaction. If the object is invalid, the
saving will fail and false will be returned.
</blockquote>

The [source for `update`](https://github.com/rails/rails/blob/93a6500baa6bbb331bb93ccdc14fdda5769f5ef9/activerecord/lib/active_record/persistence.rb#L246) goes straight into transaction code,
and is more work than is relevant for now.

To implement `update-attributes!` for a cassandra-based model,
we need to

* raise exception if the update is invalid
* fail on invalid record.


