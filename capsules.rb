#!/usr/bin/env ruby

require 'active_record'
require 'rspec/autorun'

capsules = [
  {name: 'Apollo', research: 'reentry', cost: 4, mass: 3, capacity: 3},
  {name: 'Vostok', research: 'reentry', cost: 2, mass: 2, capacity: 1},
  {name: 'Eagle', research: 'landing', cost: 4, mass: 1, capacity: 2},
  {name: 'Aldrin', research: 'life support', cost: 4, mass: 3, capacity: 8},
]

DB_SPEC = {
  adapter: 'sqlite3',
  database: ':memory:'
}.freeze
ActiveRecord::Base.establish_connection(DB_SPEC)

class Capsules < ActiveRecord::Migration[5.1]
  # TODO: maybe use change instead
  def self.up
    create_table :capsules do |t|
      t.string  :name
      t.string  :research
      t.integer :cost
      t.integer :mass
      t.integer :capacity

      t.timestamp
    end
  end

  def self.down
    drop_table :capsules
  end
end

ActiveRecord::Migrator.migrate(Capsules.up) unless Capsules.table_exists?(:capsules)

class Capsule < ActiveRecord::Base
  scope :life_support, ->{ where("research = 'life support'") }
end

capsules.each do |r|
  Capsule.create! r
end

RSpec.describe Capsule do
  context 'scopes' do
    it 'expensive' do
      expect(Capsule.life_support.pluck(:name)).to eq ['Aldrin']
    end
  end
end
