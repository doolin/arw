#!/usr/bin/env ruby

require 'active_record'
require 'rspec/autorun'

# Show that constraints triggered in db result
# in RecordNotSaved, whereas invalid models
# fire RecordInvalid
#
# What this requires is a model which has at least two attributes:
# 1. attribute which is validated.
# 2. attribute with database constraint specified in migration.
#
# Sadly, the constraints cannot be expressed directly in the rails
# migration without using pure SQL. This is really not too much of
# an issue, just means that writing this example will take longer
# than anticipated.

# Setting up the example:
#
# * `createdb arw`
# * the arw.users table may have to be dropped, definitely
#   will need to be dropped if more postgres constraints are added.
DB_SPEC = {
  :adapter => "postgresql",
  :database => "arw"
}
ActiveRecord::Base.establish_connection(DB_SPEC)

class Users < ActiveRecord::Migration[5.1]
  def self.up
    create_table :users do |t|
      t.string   :name
      t.integer  :age
    end

    # add postres constraint here
    execute "ALTER TABLE users ADD CONSTRAINT age_check CHECK (age > 0)" # NO INHERIT;
  end

  def self.down
    drop_table :users
  end
end

unless Users.table_exists?(:users)
  ActiveRecord::Migrator.migrate(Users.up)
end

# Some documentation here.
# A very simple class.
class User < ActiveRecord::Base
  validates :name, presence: true, length: { in: 4..10 }
end

describe User do
  describe '.new' do
    context 'valid' do
      it 'saves' do
        expect(User.create!(name: 'dave').valid?).to be true
      end
    end

    context 'invalid' do
      it 'raises ActiveRecord::RecordInvalid when name is too short' do
        expect do
          User.create!(name: 'foo')
        end.to raise_error(ActiveRecord::RecordInvalid)
      end

      it 'raises ActiveRecord::StatementInvalid when age less than 1' do
        expect do
          User.create!(name: 'dave', age: -1)
        end.to raise_error(ActiveRecord::StatementInvalid)
      end
    end
  end
end
