#!/usr/bin/env ruby

require 'active_record'
require 'active_support'
require 'rspec'


DB_SPEC = {
  :adapter => 'sqlite3',
  :database => ':memory:',
  #:database => 'policies.sqlite3',
  :pool => 5,
  :timeout => 5000
}
ActiveRecord::Base.establish_connection(DB_SPEC)

ActiveRecord::Base.logger = Logger.new(STDOUT)

class Policies < ActiveRecord::Migration
  def self.up
    create_table :policies do |t|
      t.string   :policy_name
    end
  end

  def self.down
    drop_table :policies
  end
end

class Rules < ActiveRecord::Migration
  def self.up
    create_table :rules do |t|
      t.string  :rule_name
      t.integer :policy_id
    end
  end

  def self.down
    drop_table :rules
  end
end

class Checks < ActiveRecord::Migration
  def self.up
    create_table :checks do |t|
      t.string :check_name
      t.integer :rule_id
    end
  end

  def self.down
    drop_table :checks
  end
end

unless Policies.table_exists?(:policies)
  ActiveRecord::Migrator.migrate(Policies.up)
end
unless Rules.table_exists?(:rules)
  ActiveRecord::Migrator.migrate(Rules.up)
end
unless Checks.table_exists?(:checks)
  ActiveRecord::Migrator.migrate(Checks.up)
end


module Logging
  extend ActiveSupport::Concern
  included do
    before_create :log_created
    before_destroy :log_destroyed
  end

  def log_created; end
  def log_destroyed; end
end

class Policy < ActiveRecord::Base
  include Logging
  has_many :rules, -> { includes :checks }

  def destroy
    Policy.transaction do
      query = "DELETE FROM checks WHERE rule_id IN (SELECT ID FROM rules WHERE policy_id = #{self.id})"
      ActiveRecord::Base.connection.execute(query)
      query = "DELETE FROM rules WHERE policy_id = #{self.id}"
      ActiveRecord::Base.connection.execute(query)
      self.delete
      super
    end
  end
end

class Rule < ActiveRecord::Base
  has_many :checks, dependent: :delete_all
  belongs_to :policy
end

class Check < ActiveRecord::Base
  belongs_to :rule
end

def make_rules
  rules = []
  (1..3).each do |r|
    rule = Rule.new
    checks = []
    (1..4).each do |c|
      checks << Check.new
    end
    rule.checks = checks
    rules << rule
  end
  rules
end

describe Policy do
  it "should fire log_created & log_destroyed" do
    policy = Policy.new
    policy.should_receive :log_created
    policy.should_receive :log_destroyed
    policy.save!
    policy.destroy
  end

  it "saves some rules" do
    policy = Policy.new
    policy.rules = make_rules
    policy.save!
    Policy.count.should eq 1
    Rule.count.should eq 3
    Check.count.should eq 12

    counter = 0
    callback = ->(*args) {
      counter += 1
    }
    ActiveSupport::Notifications.subscribed(callback, "sql.active_record") do
      policy.destroy
    end
    counter.should eq 5
    Policy.count.should eq 0
    Rule.count.should eq 0
    Check.count.should eq 0
  end
end
