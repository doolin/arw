#!/usr/bin/env ruby

# Main questions:
# 1. can we use :delete_all?
# 2. does :delete_all still log appropriately?
# 3. does :delete_all reduce the number of queries?
#
# Supporting links
#
# * http://www.zhubert.com/blog/2013/06/13/activesupport-concern-digression/
# * http://api.rubyonrails.org/classes/ActiveSupport/Concern.html#method-i-included
# * http://stackoverflow.com/questions/2797339/rails-dependent-destroy-vs-dependent-delete-all
# * http://yehudakatz.com/2009/11/12/better-ruby-idioms/
# * http://api.rubyonrails.org/classes/ActiveSupport/Notifications.html
# * http://edgeguides.rubyonrails.org/active_support_instrumentation.html
# * https://gist.github.com/doolin/6801987

require 'active_record'
require 'active_support'
require 'rspec'


module ActiveRecord
  class QueryCounter
    attr_reader :query_count

    def initialize
      @query_count = 0
    end

    def to_proc
      lambda(&method(:callback))
    end

    def callback(name, start, finish, message_id, values)
      @query_count += 1 unless %w(CACHE SCHEMA).include?(values[:name])
    end
  end
end

RSpec::Matchers.define :exceed_query_limit do |expected|
  match do |block|
    query_count(&block) > expected
  end

  failure_message_for_should_not do |actual|
    "Expected to run maximum #{expected} queries, got #{@counter.query_count}"
  end

  def query_count(&block)
    @counter = ActiveRecord::QueryCounter.new
    ActiveSupport::Notifications.subscribed(@counter.to_proc, 'sql.active_record', &block)
    @counter.query_count
  end
end

DB_SPEC = {
  :adapter => 'sqlite3',
  :database => ':memory:',
  #:database => 'policies.sqlite3',
  :pool => 5,
  :timeout => 5000
}
ActiveRecord::Base.establish_connection(DB_SPEC)

#ActiveRecord::Base.logger = Logger.new(STDOUT)

class Policies < ActiveRecord::Migration[5.1]
  def self.up
    create_table :policies do |t|
      t.string   :policy_name
    end
  end

  def self.down
    drop_table :policies
  end
end

class Rules < ActiveRecord::Migration[5.1]
  def self.up
    create_table :rules do |t|
      t.string  :rule_name
      t.integer :policy_id
    end
  end

  def self.down
    drop_table :rules
  end
end

class FooChecks < ActiveRecord::Migration[5.1]
  def self.up
    create_table :foo_checks do |t|
      t.string :check_name
      t.integer :rule_id
    end
  end

  def self.down
    drop_table :foo_checks
  end
end

class BarChecks < ActiveRecord::Migration[5.1]
  def self.up
    create_table :bar_checks do |t|
      t.string :check_name
      t.integer :rule_id
    end
  end

  def self.down
    drop_table :bar_checks
  end
end

unless Policies.table_exists?(:policies)
  ActiveRecord::Migrator.migrate(Policies.up)
end
unless Rules.table_exists?(:rules)
  ActiveRecord::Migrator.migrate(Rules.up)
end
unless FooChecks.table_exists?(:foo_checks)
  ActiveRecord::Migrator.migrate(FooChecks.up)
end
unless BarChecks.table_exists?(:bar_checks)
  ActiveRecord::Migrator.migrate(BarChecks.up)
end


module Logging
  extend ActiveSupport::Concern
  included do
    before_create :log_created
    before_destroy :log_destroyed
  end

  def log_created; end
  def log_destroyed; end
end

class Policy < ActiveRecord::Base
  include Logging
  has_many :rules, include: :foo_checks, dependent: :destroy
end

class Rule < ActiveRecord::Base
  has_many :foo_checks, dependent: :destroy
  #has_many :foo_checks, dependent: :delete_all
  #has_many :bar_checks, dependent: :delete_all
  belongs_to :policy
end

class FooCheck < ActiveRecord::Base
  belongs_to :rule
end

class BarCheck < ActiveRecord::Base
  belongs_to :rule
end

describe Policy do
  it "should fire log_created & log_destroyed" do
    policy = Policy.new
    policy.should_receive :log_created
    policy.should_receive :log_destroyed

    expect {
      policy.save!
      policy.destroy
    }.to_not exceed_query_limit(7)
  end

  it "saves some rules" do
    policy = Policy.new
    rules = []
    (1..226).each do |r|
      rule = Rule.new
      checks = []
      (1..2).each do |c|
        checks << FooCheck.new
      end
      rule.foo_checks = checks
      rules << rule
    end
    policy.rules = rules
    policy.should_receive :log_created
    policy.should_receive :log_destroyed
    policy.save!

    counter = 0
    callback = ->(*args) { counter += 1 }
    ActiveSupport::Notifications.subscribed(callback, "sql.active_record") do
      policy.destroy
    end
    counter.should eq 23
    Policy.count.should eq 0
    Rule.count.should eq 0
    FooCheck.count.should eq 0
  end
end
