#!/usr/bin/env ruby

require 'active_record'
require 'active_support'
require 'rspec'


DB_SPEC = {
  :adapter => 'sqlite3',
  :database => ':memory:',
  #:database => 'policies.sqlite3',
  :pool => 5,
  :timeout => 5000
}
ActiveRecord::Base.establish_connection(DB_SPEC)

ActiveRecord::Base.logger = Logger.new(STDOUT)

class Policies < ActiveRecord::Migration
  def self.up
    create_table :policies do |t|
      t.string   :policy_name
    end
  end

  def self.down
    drop_table :policies
  end
end

class Rules < ActiveRecord::Migration
  def self.up
    create_table :rules do |t|
      t.string  :rule_name
      t.integer :policy_id
    end
  end

  def self.down
    drop_table :rules
  end
end

class FooChecks < ActiveRecord::Migration
  def self.up
    create_table :foo_checks do |t|
      t.string :check_name
      t.integer :rule_id
    end
  end

  def self.down
    drop_table :foo_checks
  end
end

class BarChecks < ActiveRecord::Migration
  def self.up
    create_table :bar_checks do |t|
      t.string :check_name
      t.integer :rule_id
    end
  end

  def self.down
    drop_table :bar_checks
  end
end

unless Policies.table_exists?(:policies)
  ActiveRecord::Migrator.migrate(Policies.up)
end
unless Rules.table_exists?(:rules)
  ActiveRecord::Migrator.migrate(Rules.up)
end
unless FooChecks.table_exists?(:foo_checks)
  ActiveRecord::Migrator.migrate(FooChecks.up)
end
unless BarChecks.table_exists?(:bar_checks)
  ActiveRecord::Migrator.migrate(BarChecks.up)
end


module Logging
  extend ActiveSupport::Concern
  included do
    before_create :log_created
    before_destroy :log_destroyed
  end

  def log_created; end
  def log_destroyed; end
end

class Policy < ActiveRecord::Base
  include Logging
  has_many :rules, -> { includes :foo_checks, :bar_checks }, dependent: :destroy

  def destroy
    Policy.transaction do
      query = "delete from checks where rule_id in (select id from rules where csm_policy_id = #{self.id})"
      ActiveRecord::Base.connection.execute(query)
      query = "delete from rules where csm_policy_id = #{self.id}"
      ActiveRecord::Base.connection.execute(query)
      self.delete
      super
    end
  end
end

class Rule < ActiveRecord::Base
  has_many :foo_checks, dependent: :destroy
  has_many :bar_checks, dependent: :delete_all
  belongs_to :policy
end

class FooCheck < ActiveRecord::Base
  belongs_to :rule
end

class BarCheck < ActiveRecord::Base
  belongs_to :rule
end

def make_rules
  rules = []
  (1..3).each do |r|
    rule = Rule.new
    foo_checks = []
    bar_checks = []
    (1..1).each do |c|
      foo_checks << FooCheck.new
      bar_checks << BarCheck.new
    end
    rule.foo_checks = foo_checks
    rule.bar_checks = bar_checks
    rules << rule
  end
  rules
end

describe Policy do
  xit "should fire log_created & log_destroyed" do
    policy = Policy.new
    policy.should_receive :log_created
    policy.should_receive :log_destroyed
    policy.save!
    policy.destroy
  end

  it "saves some rules" do
    policy = Policy.new
    rules = []
    (1..3).each do |r|
      rule = Rule.new
      foo_checks = []
      bar_checks = []
      (1..1).each do |c|
        foo_checks << FooCheck.new
        bar_checks << BarCheck.new
      end
      rule.foo_checks = foo_checks
      rule.bar_checks = bar_checks
      rules << rule
    end
    policy.rules = rules
    policy.should_receive :log_created
    policy.should_receive :log_destroyed
    policy.save!

    counter = 0
    callback = ->(*args) { counter += 1 }
    ActiveSupport::Notifications.subscribed(callback, "sql.active_record") do
      policy.destroy
    end
    counter.should eq 9
    Policy.count.should eq 0
    Rule.count.should eq 0
    FooCheck.count.should eq 0
  end
end
