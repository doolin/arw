#!/usr/bin/env ruby

require 'active_record'
require 'rspec/autorun'

rockets = [
  {name: 'Saturn', cost: 15, mass: 20, thrust: 200},
  {name: 'Soyuz', cost: 8, mass: 9, thrust: 80},
  {name: 'Atlas', cost: 5, mass: 4, thrust: 27},
  {name: 'Juno', cost: 1, mass: 1, thrust: 4},
]

DB_SPEC = {
  adapter: 'sqlite3',
  database: ':memory:'
}.freeze
ActiveRecord::Base.establish_connection(DB_SPEC)

class Rockets < ActiveRecord::Migration[5.1]
  # TODO: maybe use change instead
  def self.up
    create_table :rockets do |t|
      t.string  :name
      t.integer :cost
      t.integer :mass
      t.integer :thrust

      t.timestamp
    end
  end

  def self.down
    drop_table :rockets
  end
end

ActiveRecord::Migrator.new(:up, Rockets.up).migrate unless Rockets.table_exists?(:rockets)

class Rocket < ActiveRecord::Base
  scope :expensive, ->{ where("cost > 8") }
end

rockets.each do |r|
  Rocket.create! r
end

RSpec.describe Rocket do
  context 'scopes' do
    it 'expensive' do
      expect(Rocket.expensive.pluck(:name)).to eq ['Saturn']
    end
  end
end
