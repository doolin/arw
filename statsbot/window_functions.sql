-- https://blog.statsbot.co/sql-window-functions-tutorial-b5075b87d129
-- postgres=# \i statsbot/window_functions.sql


drop database if exists windowfunctions;
CREATE DATABASE windowfunctions;
\c windowfunctions
CREATE SCHEMA wf;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = wf, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

create table orders (
  order_id integer not null,
  customer_id char(1) not null,
  state char(2) not null,
  ordered_at timestamp without time zone NOT NULL,
  amount integer not null
);

insert into orders (order_id, customer_id, state, ordered_at, amount) values (1, 'A', 'CA', '2017-01-01 00:00:00.00000', 200);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (2, 'B', 'CA', '2017-01-05 00:00:00.00000', 250);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (3, 'C', 'NY', '2017-01-12 00:00:00.00000', 200);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (4, 'A', 'CA', '2017-02-04 00:00:00.00000', 400);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (5, 'D', 'CA', '2017-02-05 00:00:00.00000', 250);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (5, 'D', 'CA', '2017-02-05 12:00:00.00000', 300);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (6, 'C', 'NY', '2017-02-19 00:00:00.00000', 300);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (7, 'A', 'CA', '2017-03-01 00:00:00.00000', 150);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (8, 'E', 'NY', '2017-03-05 00:00:00.00000', 500);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (9, 'F', 'CA', '2017-03-09 00:00:00.00000', 250);
insert into orders (order_id, customer_id, state, ordered_at, amount) values (10, 'B', 'CA', '2017-03-11 00:00:00.00000', 600);

select date_trunc('month', ordered_at) as month, sum(amount) as revenue
  from orders
  group by 1
  order by 1;

--         month        | revenue
        ---------------------+---------
--  2017-01-01 00:00:00 |     650
--  2017-02-01 00:00:00 |    1250
--  2017-03-01 00:00:00 |    1500

/*
 * Need to implement datediff in postgres.
 * This is also worth implementing in SQlite
 * is it has no window functions and the following
 * is otherwise pure sql (modulo datediff).
 * Here's an interesting link:
 * http://www.sqlines.com/postgresql/how-to/datediff
 */
/*
with monthly_revenue as (
select date_trunc('month', ordered_at)::date as month,
sum(amount) as revenue
  from orders
  group by 1
), previous_month_revenue as (
select t1.*, t2.revenue as previous_month_revenue
  from monthly_revenue t1
  left join monthly_revenue t2
  -- datediff is a SQL Server thing, won't work in postgres
  on datediff('month', t2.month, t1.month) = 1
) select *, round(100.0 * (revenue - previous_month_revenue)/previous_month_revenue, 1) as
revenue_growth from previous_month_revenue
order by 1;
*/

with monthly_revenue as (
  select date_trunc('month', ordered_at)::date as month,
    sum(amount) as revenue
  from orders
  group by 1
), previous_monthly_revenue as (
  select *,
  lag(revenue) over (order by month) as previous_monthly_revenue
  from monthly_revenue
) select *,
  round(100.0 * (revenue - previous_monthly_revenue)/previous_monthly_revenue, 1) as revenue_growth
  from previous_monthly_revenue
  order by 1;

--    month    | revenue | previous_monthly_revenue | revenue_growth
--   ------------+---------+--------------------------+----------------
--  2017-01-01 |     650 |                          |
--  2017-02-01 |    1250 |                      650 |           92.3
--  2017-03-01 |    1500 |                     1250 |           20.0
-- (3 rows)


with monthly_revenue as (
  select date_trunc('month', ordered_at)::date as month,
  state,
  sum(amount) as revenue
  from orders
  group by 1, 2
), previous_monthly_revenue as (
  select *,
  lag(revenue) over (order by month) as previous_monthly_revenue
  from monthly_revenue
) select *,
  round(100.0 * (revenue - previous_monthly_revenue)/previous_monthly_revenue, 1) as revenue_growth
  from previous_monthly_revenue
  order by 2, 1;

--    month    | state | revenue | previous_monthly_revenue | revenue_growth
--   ------------+-------+---------+--------------------------+----------------
--  2017-01-01 | CA    |     450 |                      200 |          125.0
--  2017-02-01 | CA    |     950 |                      450 |          111.1
--  2017-03-01 | CA    |    1000 |                      300 |          233.3
--  2017-01-01 | NY    |     200 |                          |
--  2017-02-01 | NY    |     300 |                      950 |          -68.4
--  2017-03-01 | NY    |     500 |                     1000 |          -50.0
-- (6 rows)

-- Running totals for a period of time.
with monthly_revenue as (
  select date_trunc('month', ordered_at)::date as month,
  sum(amount) as revenue
  from orders
  group by 1
) select *,
  sum(revenue) over (order by month rows between unbounded
    preceding and current row) as running_total
  from monthly_revenue
  order by 1;

--    month    | revenue | running_total
-- ------------+---------+---------------
--  2017-01-01 |     650 |           650
--  2017-02-01 |    1250 |          1900
--  2017-03-01 |    1500 |          3400
-- (3 rows)

-- Commenting out the following to help get buffer space
-- in terminal for debugging next query.
/*
select
  *,
  sum(amount) over () as amount_total,
  sum(amount) over (order by order_id rows between unbounded preceding and current row) as running_sum ,
  sum(amount) over (partition by customer_id order by ordered_at rows between unbounded preceding and current row) as running_sum_by_customer,
  avg(amount) over (order by ordered_at rows between 5 preceding and current row) as trailing_average
  from orders
  order by 1;
*/

--  order_id | customer_id | state |     ordered_at      | amount | amount_total | running_sum | running_sum_by_customer |   trailing_average
-- ----------+-------------+-------+---------------------+--------+--------------+-------------+-------------------------+----------------------
--         1 | A           | CA    | 2017-01-01 00:00:00 |    200 |         3400 |         200 |                     200 | 200.0000000000000000
--         2 | B           | CA    | 2017-01-05 00:00:00 |    250 |         3400 |         450 |                     250 | 225.0000000000000000
--         3 | C           | NY    | 2017-01-12 00:00:00 |    200 |         3400 |         650 |                     200 | 216.6666666666666667
--         4 | A           | CA    | 2017-02-04 00:00:00 |    400 |         3400 |        1050 |                     600 | 262.5000000000000000
--         5 | D           | CA    | 2017-02-05 00:00:00 |    250 |         3400 |        1300 |                     250 | 260.0000000000000000
--         5 | D           | CA    | 2017-02-05 12:00:00 |    300 |         3400 |        1600 |                     550 | 266.6666666666666667
--         6 | C           | NY    | 2017-02-19 00:00:00 |    300 |         3400 |        1900 |                     500 | 283.3333333333333333
--         7 | A           | CA    | 2017-03-01 00:00:00 |    150 |         3400 |        2050 |                     750 | 266.6666666666666667
--         8 | E           | NY    | 2017-03-05 00:00:00 |    500 |         3400 |        2550 |                     500 | 316.6666666666666667
--         9 | F           | CA    | 2017-03-09 00:00:00 |    250 |         3400 |        2800 |                     250 | 291.6666666666666667
--        10 | B           | CA    | 2017-03-11 00:00:00 |    600 |         3400 |        3400 |                     850 | 350.0000000000000000
-- (11 rows)

-- Duplicate data
select * from (
  select *, row_number() over (partition by order_id order by ordered_at desc)
  from orders
) as cleaned
where row_number = 1;

--  order_id | customer_id | state |     ordered_at      | amount | row_number
-- ----------+-------------+-------+---------------------+--------+------------
--         1 | A           | CA    | 2017-01-01 00:00:00 |    200 |          1
--         2 | B           | CA    | 2017-01-05 00:00:00 |    250 |          1
--         3 | C           | NY    | 2017-01-12 00:00:00 |    200 |          1
--         4 | A           | CA    | 2017-02-04 00:00:00 |    400 |          1
--         5 | D           | CA    | 2017-02-05 12:00:00 |    300 |          1
--         6 | C           | NY    | 2017-02-19 00:00:00 |    300 |          1
--         7 | A           | CA    | 2017-03-01 00:00:00 |    150 |          1
--         8 | E           | NY    | 2017-03-05 00:00:00 |    500 |          1
--         9 | F           | CA    | 2017-03-09 00:00:00 |    250 |          1
--        10 | B           | CA    | 2017-03-11 00:00:00 |    600 |          1
-- (10 rows)


-- original data with row numbers computed by row_number()
select *,
  row_number() over (partition by order_id order by ordered_at desc)
  from orders;

--  order_id | customer_id | state |     ordered_at      | amount | row_number
-- ----------+-------------+-------+---------------------+--------+------------
--         1 | A           | CA    | 2017-01-01 00:00:00 |    200 |          1
--         2 | B           | CA    | 2017-01-05 00:00:00 |    250 |          1
--         3 | C           | NY    | 2017-01-12 00:00:00 |    200 |          1
--         4 | A           | CA    | 2017-02-04 00:00:00 |    400 |          1
--         5 | D           | CA    | 2017-02-05 12:00:00 |    300 |          1
--         5 | D           | CA    | 2017-02-05 00:00:00 |    250 |          2
--         6 | C           | NY    | 2017-02-19 00:00:00 |    300 |          1
--         7 | A           | CA    | 2017-03-01 00:00:00 |    150 |          1
--         8 | E           | NY    | 2017-03-05 00:00:00 |    500 |          1
--         9 | F           | CA    | 2017-03-09 00:00:00 |    250 |          1
--        10 | B           | CA    | 2017-03-11 00:00:00 |    600 |          1
-- (11 rows)


with orders_cleaned as (
  select * from (
    select *, row_number() over (partition by order_id order by ordered_at desc)
    from orders
  ) as cleaned
  where row_number = 1
), monthly_revenue as (
  select date_trunc('month', ordered_at)::date as month,
       sum(amount) as revenue
  from orders_cleaned
  group by 1
), prev_month_revenue as (
  select *,
  lag(revenue) over (order by month) as prev_month_revenue
  from monthly_revenue
) select *,
  round(100.0*(revenue - prev_month_revenue)/prev_month_revenue, 1) as revenue_growth
  from prev_month_revenue
  order by 1;

--    month    | revenue | prev_month_revenue | revenue_growth
--   ------------+---------+--------------------+----------------
--  2017-01-01 |     650 |                    |
--  2017-02-01 |    1000 |                650 |           53.8
--  2017-03-01 |    1500 |               1000 |           50.0
-- (3 rows)

create view orders_cleaned as
  select order_id, customer_id, state, ordered_at, amount
  from (
    select *,
    row_number() over (partition by order_id order by ordered_at desc)
    from orders
  ) as cleaned
  where row_number = 1;

select * from orders_cleaned;

-- Result is as above.

-- Top N rows in every group:


------------ From Thoughtbot ----------------

/*
CREATE TABLE posts (
  id integer PRIMARY KEY,
  body varchar,
  created_at timestamp DEFAULT current_timestamp
);

CREATE TABLE comments (
 id INTEGER PRIMARY KEY,
 post_id integer NOT NULL,
 body varchar,
 created_at timestamp DEFAULT current_timestamp
);

INSERT INTO posts VALUES (1, 'foo');
INSERT INTO posts VALUES (2, 'bar');

INSERT INTO comments VALUES (1, 1, 'foo old');
INSERT INTO comments VALUES (2, 1, 'foo new');
INSERT INTO comments VALUES (3, 1, 'foo newer');
INSERT INTO comments VALUES (4, 1, 'foo newest');

INSERT INTO comments VALUES (5, 2, 'bar old');
INSERT INTO comments VALUES (6, 2, 'bar new');
INSERT INTO comments VALUES (7, 2, 'bar newer');
INSERT INTO comments VALUES (8, 2, 'bar newest');
*/



\c postgres
