#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_record'
require 'pry'

require 'rspec/autorun'

DB_SPEC = {
  adapter: 'sqlite3',
  database: ':memory:'
}.freeze
ActiveRecord::Base.establish_connection(DB_SPEC)

class Puzzles < ActiveRecord::Migration[5.1]
  def change
    create_table :puzzles do |t|
      t.string :name
      t.timestamp
    end
  end
end

class Persons < ActiveRecord::Migration[5.1]
  def change
    create_table :persons do |t|
      t.string :name
      t.timestamps
    end
  end
end

class Builders < ActiveRecord::Migration[5.1]
  def change
    create_table :builders do |t|
      t.belongs_to :person, index: true
      t.belongs_to :puzzle, index: true
      t.datetime :completed_at
      t.integer :percent_complete
      t.timestamps
    end
  end
end

ActiveRecord::Migrator.migrate(Puzzles.new.change) unless Puzzles.table_exists?(:puzzles)
ActiveRecord::Migrator.migrate(Persons.new.change) unless Persons.table_exists?(:persons)
ActiveRecord::Migrator.migrate(Builders.new.change) unless Builders.table_exists?(:builders)

class Puzzle < ActiveRecord::Base
  validates :name, presence: true

  has_many :builders
  has_many :persons, through: :builders

  scope :built_by, ->(name) { joins(:persons).where(persons: { name: name }).distinct(:name).pluck(:name) }
end

class Person < ActiveRecord::Base
  self.table_name = 'persons'

  validates :name, presence: true

  has_many :builders
  has_many :puzzles, through: :builders

  scope :built_puzzles, ->(name) { joins(:puzzles).where(puzzles: { name: name }).pluck(:name) }
end

class Builder < ActiveRecord::Base
  belongs_to :puzzle
  belongs_to :person
end

dave = Person.create!(name: 'dave')
shelley = Person.create!(name: 'shelley')
miles = Person.create!(name: 'miles')

_boats = Puzzle.create!(name: 'boats', persons: [dave])
_boats2 = Puzzle.create!(name: 'boats', persons: [dave])
_cars = Puzzle.create!(name: 'cars', persons: [dave, shelley])
_art = Puzzle.create!(name: 'art', persons: [dave, shelley, miles])
_planes = Puzzle.create!(name: 'planes', persons: [shelley, miles])

RSpec.describe Person do
  describe '.create!' do
    example 'new person' do
      expect(Person.find_by(name: 'dave').name).to eq 'dave'
    end
  end

  describe '.built_puzzles' do
    example 'by name' do
      expected = %w[dave shelley]
      actual = Person.built_puzzles('cars')
      expect(actual).to eq expected
    end
  end
end

RSpec.describe Puzzle do
  describe '.built_by' do
    it 'shows puzzles person has built' do
      expected = %w[boats cars art]
      actual = Puzzle.built_by('dave')
      expect(actual).to match_array(expected)
    end
  end
end

RSpec.describe Builder do
  xit 'finds puzzles built by two people' do
    # shelley and miles only
    # result = Builder
    # binding.pry
  end
end

# More questions to ask:
# * How many times has a person built some particular puzzle?
# * Which  puzzles are two person only?
# * Which puzzles were built by only two distinct people?
