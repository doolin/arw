#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_record'
require 'pry'

require 'rspec/autorun'

DB_SPEC = {
  adapter: 'sqlite3',
  database: ':memory:'
}.freeze
ActiveRecord::Base.establish_connection(DB_SPEC)

class Books < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :name
      t.timestamp
    end
  end
end

class Persons < ActiveRecord::Migration[5.1]
  def change
    create_table :persons do |t|
      t.string :name
      t.timestamps
    end
  end
end

class Readers < ActiveRecord::Migration[5.1]
  def change
    create_table :readers do |t|
      t.belongs_to :person, index: true
      t.belongs_to :book, index: true
      t.datetime :completed_at
      t.integer :percent_complete
      t.timestamps
    end
  end
end

ActiveRecord::Migrator.migrate(Books.new.change) unless Books.table_exists?(:books)
ActiveRecord::Migrator.migrate(Persons.new.change) unless Persons.table_exists?(:persons)
ActiveRecord::Migrator.migrate(Readers.new.change) unless Readers.table_exists?(:readers)

class Book < ActiveRecord::Base
  validates :name, presence: true

  has_many :readers
  has_many :persons, through: :readers

  scope :built_by, ->(name) { joins(:persons).where(persons: { name: name }).distinct(:name).pluck(:name) }
end

class Person < ActiveRecord::Base
  self.table_name = 'persons'

  validates :name, presence: true

  has_many :readers
  has_many :books, through: :readers

  scope :built_books, ->(name) { joins(:books).where(books: { name: name }).pluck(:name) }
end

class Reader < ActiveRecord::Base
  belongs_to :book
  belongs_to :person
end

dave = Person.create!(name: 'dave')
shelley = Person.create!(name: 'shelley')
miles = Person.create!(name: 'miles')

_boats = Book.create!(name: 'boats', persons: [dave])
_boats2 = Book.create!(name: 'boats', persons: [dave])
_cars = Book.create!(name: 'cars', persons: [dave, shelley])
_art = Book.create!(name: 'art', persons: [dave, shelley, miles])
_planes = Book.create!(name: 'planes', persons: [shelley, miles])

RSpec.describe Person do
  describe '.create!' do
    example 'new person' do
      expect(Person.find_by(name: 'dave').name).to eq 'dave'
    end
  end

  describe '.built_books' do
    example 'by name' do
      expected = %w[dave shelley]
      actual = Person.built_books('cars')
      expect(actual).to eq expected
    end
  end
end

RSpec.describe Book do
  describe '.built_by' do
    it 'shows books person has built' do
      expected = %w[boats cars art]
      actual = Book.built_by('dave')
      expect(actual).to match_array(expected)
    end
  end
end

RSpec.describe Reader do
  xit 'finds books built by two people' do
    # shelley and miles only
    # result = Reader
    # binding.pry
  end
end

# More questions to ask:
# * How many times has a person built some particular book?
# * Which  books are two person only?
# * Which books were built by only two distinct people?
