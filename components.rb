class Components
  Supplies = {
    mass: 1,
    research: 'life support',
    cost: 5
  }

  IonThruster = {
    mass: 1,
    research: 'ion thruster',
    cost: 5
  }

  Probe = {
    mass: 1,
    cost: 2
  }
end
