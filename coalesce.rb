#!/usr/bin/env ruby

# Postgres COALESCE demo

require 'active_record'
require 'rspec/autorun'

DB_SPEC = {
  :adapter => "postgresql",
  :database => "arw"
}
ActiveRecord::Base.establish_connection(DB_SPEC)

class Users < ActiveRecord::Migration[5.1]
  def self.up
    create_table :users do |t|
      t.string   :name
      t.integer  :age
    end

    # add postres constraint here
    # execute "ALTER TABLE users ADD CONSTRAINT age_check CHECK (age > 0)" # NO INHERIT;
  end

  def self.down
    drop_table :users
  end
end

unless Users.table_exists?(:users)
  ActiveRecord::Migrator.migrate(Users.up)
end

class User < ActiveRecord::Base
end

describe User do
end
